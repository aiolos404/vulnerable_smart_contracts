# README #

All the source code are downloaded from https://etherscan.io/contractsVerified

Vulnerability types and definitions section shows the definitions of each type of vulnerabilities.

Vulnerable smart contracts in each type set the some smart contracts for each type

Explaination of vulnerability in each smart contracts section gives details of why there's vulnerability in each smart contract.


### Vulnerability types and definitions###

* Respect the Limit: Allow users to set an expected limit on the amount of Ether transferred out of the contract’s wallet. We use a symbolic variable to simulate the remaining limit, which is initialized to be the limit. Each time an amount is transferred out, we decrease the variable by the same amount. Afterwards, we check whether the remaining limit (i.e., a symbolic expression) is less than zero. If it is, we conclude that the program path potentially violates the property. 

* Avoid Non-Existing Addresses: For every program path which contains instruction CALL or SELFDESTRUCT, sCompile checks whether the address in the instruction exists or not. This is done with the help of EtherScan, which is a block explorer, search, API and analytic platform for Ethereum.

* Guard Suicide: sCompile checks whether a program path would result in destructing the contract without constraints on the date or block number, or the contract ownership. If however a program path which executes the opcode instruction SELFDESTRUCT can be executed without constraints on the date or block number, or the contract ownership, the contract can be destructed arbitrarily and the Ether in the wallet can be transferred to anyone. We thus check whether there exists a program path which executes SELFDESTRUCT and whether its path condition is constituted with constraints on date or block number and contract owner address. While checking the former is straightforward, checking the latter is achieved by checking whether the path contains constraints on instruction TIMESTAMP or BLOCK, and checking whether the path condition compares the variables representing the contract owner address with other addresses.

* Be No Black Hole: sCompile check weather smart contract only accept ethers but never send any ether out of the contracts. sCompile analyzes program paths which do not contain CALL, CREATE, DELEGATECALL or SELFDESTRUCT. For instance, if a contract has no money-related paths (i.e., never sends any Ether out), sCompile then checks whether there exists a program path which allows the contract to receive Ether.


### Vulnerable smart contracts in each type ###

* Respect the Limit: toyDAO.sol

* Avoid Non-Existing Addresses: EnjinBuyer.sol

* Guard Suicide: StandardToken.sol, ViewTokenMintage.sol, MiCarsToken.sol, Mortal.sol

* Be No Black Hole: Bitway.sol, GigsToken.sol


### Explaination of vulnerability in each smart contracts ###

* toyDAO.sol: like DAO attack. Mapping credit is a map which records a user’s credit amount. Function donate() allows a user to top up its credit with 100 wei (which is a unit of Ether). Function withdraw() by design sends 20 wei to the message sender (at line 11) and then updates credit accordingly. However, when line 11 is executed, the message sender could call function withdraw() through its fallback function, before line 12 is executed. As a result, line 11 is executed again and another 20 wei is sent to the message sender. Eventually, all Ether in the wallet of this contract will be sent to the message sender.

* EnjinBuyer.sol: It has 2 inherent addresses for developer and sale in line 20 and 21 respectively. In function purchase_tokens(), the balance is sent to the sale’s address. There is a mistake on the sale’s address and as a result the balance is sent to a non-existing address and is lost forever. Note that any hexadecimal string of length not greater than 40 is considered a valid (well-formed) address in Ethereum and thus there is no error when function purchase_tokens() is executed.

* StandardToken.sol: selfdestruct() at line 39 must be guarded by the date or block number, or the contract ownership.

* ViewTokenMintage.sol: guard of selfdestruct depends on the return value of function isAuthorized(). The path going through line 284 returns true only if the msg.sender is the same as the current contract. sCompile mistakenly reports the alarm as the ADDRESS is symbolized as a symbolic constant.

* MiCarsToken.sol: At line 60, there are 2 constraints before selfdestruct in the contract. sCompile considers such a contract safe for there is a guard of msg.sender == owner (or the other condition), whereas MAIAN reports a vulnerability as the contract can also be killed if the msg.sender is not the owner when the second condition is satisfied.

* Mortal.sol: In this contract, selfdestruct is well guarded, but the developer makes a mistake so that the constructor becomes a normal function, and anyone can invoke mortal() to make himself the owner of this contract and kill the contract.

* Bitway.sol: It receives Ether (i.e., cryptocurrency in Ethereum) through function createTokens(). Note that this is possible because function createTokens() is declared as payable. However, there is no function in the contract which can send Ether out. The most critical one is a program path where function createTokens() is invoked.

* GigsToken.sol: This contract shows a contract which is capable of both receiving (since the function is payable) and sending Ether (due to owner.transfer(msg.value) at line 74), and thus sCompile does not flag it to be a black hole contract. MAIAN however claims that it is. A closer investigation reveals that because MAIAN has trouble in solving the path condition for reaching line 74, and thus mistakenly assumes that the path is infeasible. As a result, it believes that there is no way Ethers can be sent out and thus the contract is “greedy”.



