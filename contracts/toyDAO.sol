contract toyDAO{
  address owner;
  mapping (address => uint) credit;
  function toyDAO() payable public {
    owner = msg.sender;
  }
  function donate() payable public{
    credit[msg.sender] = 100;
  }
  function withdraw() public {
    uint256 value = 20; 
    if (msg.sender.call.value(value)()) {
      credit[msg.sender] = credit[msg.sender] - value;
    } 
  }
}